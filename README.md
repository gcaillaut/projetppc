usage: projet04.py [-h] [-o OUT] [-s] [-p PATH] [problems [problems ...]]

Génère et exécute des fichiers .mzn

positional arguments:
  problems              Liste des problèmes à générer

optional arguments:
  -h, --help            show this help message and exit
  -o OUT, --out OUT     Dossier de destination
  -s, --solve           Résoudre les problèmes sélectionnés
  -p PATH, --path PATH  Chemin vers l'exécutable mzn-gecode
