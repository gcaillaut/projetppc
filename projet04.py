import re
affectation = re.compile(r"^(\w+)\s*=\s*(\d+)\s*$")
agent = re.compile(r"^\s*Agent \(\d+\): (.+)$")
row = re.compile(r"^\s*\d+:\s*(.+)$")

PROBLEMES = []

for pb in open("projet04.txt").read().split("===============================================================================\n"):
    pb = pb.strip()
    if not pb:
        continue
    lines = pb.split("\n")
    lines.reverse()
    lines.pop()
    lines.pop()
    d = {}
    PROBLEMES.append(d)
    while lines:
        l = lines.pop()
        m = affectation.match(l)
        if not m:
            break
        d[m.group(1)] = int(m.group(2))
    while lines:
        if l.startswith("Reunions Par Agent"):
            break
        l = lines.pop()
    reunions_par_agent = []
    d["ReunionsParAgent"] = reunions_par_agent
    while lines:
        l = lines.pop()
        m = agent.match(l)
        if not m:
            break
        reunions_par_agent.append([int(x) for x in m.group(1).strip().split()])
    while lines:
        if l.startswith("Temps De Deplacement Entre Reunions"):
            lines.pop()
            break
        l = lines.pop()
    temps_de_deplacement = []
    d["TempsDeDeplacement"] = temps_de_deplacement
    while lines:
        l = lines.pop()
        m = row.match(l)
        if m:
            temps_de_deplacement.append([int(x) for x in m.group(1).strip().split()])

import sys, argparse, os
# import yaml, sys
# yaml.dump(PROBLEMES, sys.stderr)

def write_const(varType, name, dict):
    return "%s: %s = %s;" % (varType, name, dict[name])

def write_const_matrix(len1, len2, varType, name, dict):
    valuesStr = "\n|".join([",".join([str(y) for y in x]) for x in dict[name]])
    return "array [1..%d, 1..%d] of %s: %s = \n[|%s\n|];" % (len1, len2, varType, name, valuesStr)

def write_const_array_set(len, varType, name, dict):
    valuesStr = ",\n".join(["{" + ",".join([str(y) for y in x]) + "}" for x in dict[name]])
    return "array [1..%d] of set of %s: %s = \n[\n%s\n];" % (len, varType, name, valuesStr)

def write_file(problem, number, out_dir):
    vars = [
        write_const("int", MaxTempsDeDeplacement, problem),
        write_const("int", MinTempsDeDeplacement, problem),
        write_const("int", NombreDAgents, problem),
        write_const("int", NombreDeReunions, problem),
        write_const("int", NombreDeReunionsParAgent, problem),
        write_const_matrix(problem[NombreDAgents], problem[NombreDeReunionsParAgent], "int", ReunionsParAgent, problem),
        # write_const_array_set(problem[NombreDAgents], "int", ReunionsParAgent, problem),
        write_const("int", TaillePlageDeTemps, problem),
        write_const_matrix(problem[NombreDeReunions], problem[NombreDeReunions], "int", TempsDeDeplacement, problem)
    ]

    filename = os.path.join(out_dir, "probleme%d.mzn" % number)
    with open(filename, "w") as f:
        f.write("\n".join(vars))

if __name__ == "__main__":
    MaxTempsDeDeplacement = "MaxTempsDeDeplacement"
    MinTempsDeDeplacement = "MinTempsDeDeplacement"
    NombreDAgents = "NombreDAgents"
    NombreDeReunions = "NombreDeReunions"
    NombreDeReunionsParAgent = "NombreDeReunionsParAgent"
    ReunionsParAgent = "ReunionsParAgent"
    TaillePlageDeTemps = "TaillePlageDeTemps"
    TempsDeDeplacement = "TempsDeDeplacement"

    parser = argparse.ArgumentParser(description="Génère et exécute des fichiers .mzn"
                                     + '\n'
                                     + "Exemple : python %s -p path/vers/dossier/mzn-g12lazy -o dossier/destination --solve 0 1 2"
 % (sys.argv[0]))
    parser.add_argument("-o", "--out",
                        default=".",
                        type=str,
                        help="Dossier de destination")
    parser.add_argument("problems",
                        default=range(len(PROBLEMES)), nargs="*",
                        type=int,
                        help="Liste des problèmes à générer")
    parser.add_argument("-s", "--solve",
                        action="store_true",
                        help="Résoudre les problèmes sélectionnés")
    parser.add_argument("-p", "--path",
                        type=str,
                        help="Chemin vers l'exécutable mzn-gecode")
    
    args = parser.parse_args()

    for i in args.problems:
        try:
            idx = int(i)
            write_file(PROBLEMES[idx], idx, args.out)
        except ValueError:
            print("'%s' is not an integer" % i, file=sys.stderr)
        except IndexError:
            print("%d is out of bounds (max: %d)" % (idx, len(PROBLEMES)-1), file=sys.stderr)

    if args.solve:
        from string import Template
        import subprocess

        if args.path is not None:
            os.environ["PATH"] = "%s:%s" % (os.environ["PATH"], args.path)
        
        with open("projet04.mzn.template", "r") as f:
            template = Template(f.read())
        
        for i in args.problems:
            complete_filename = os.path.join(args.out, "complete_probleme%d.mzn" % i)
            with open(complete_filename, "w") as f:
                f.write(template.substitute(inputfile="probleme%d.mzn" % i))
                
            subprocess.call(["mzn-g12lazy", complete_filename])
        
